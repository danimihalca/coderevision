#include "CodeRevisionControllerImpl.hpp"

#include "repository/FileProgrammerRepository.hpp"
#include "repository/FileSourceFileRepository.hpp"

#include <iostream>

CodeRevisionControllerImpl::CodeRevisionControllerImpl()
    : m_programmerRepo(new FileProgrammerRepository("programmers.txt"))
    , m_sourceFileRepo(new FileSourceFileRepository("source_files.txt"))
{
}

const std::vector<SourceFile>& CodeRevisionControllerImpl::getSourceFiles()
{
    return m_sourceFileRepo->getSourceFiles();
}

const std::vector<Programmer>& CodeRevisionControllerImpl::getProgrammers()
{
    return m_programmerRepo->getProgrammers();
}

void CodeRevisionControllerImpl::addSourceFile(const std::string& fileName, const Programmer& programmer)
{
    if (m_sourceFileRepo->sourceFileExists(fileName))
    {
        //TODO: handle in UI, return false/true
        std::cout << "Source file already exists" << std::endl;
    }

    SourceFile sourceFile(fileName, SourceFileStatus::NOT_REVISED, programmer.getName(), "");

    m_sourceFileRepo->addSourceFile(sourceFile);
}

void CodeRevisionControllerImpl::reviewSourceFile(const std::string& fileName, const Programmer& programmer)
{
    //TODO: also check if it is already reviewed
    if (!m_sourceFileRepo->sourceFileExists(fileName))
    {
        //TODO: handle in UI, return false/true
        std::cout << "Source file doesnt exist for review" << std::endl;
    }

    SourceFile sourceFile(fileName, SourceFileStatus::REVISED, "", programmer.getName());

    m_sourceFileRepo->updateSourceFile(sourceFile);
    //TODO: update programmer in repo (counters for reviewed/unreviewed files)
}
