#ifndef CODEREVISIONCONTROLLERIMPL_HPP
#define CODEREVISIONCONTROLLERIMPL_HPP

#include <memory>

#include "CodeRevisionControllerInterface.hpp"

#include "repository/ProgrammerRepositoryInterface.hpp"
#include "repository/SourceFileRepositoryInterface.hpp"

class CodeRevisionControllerImpl : public CodeRevisionControllerInterface
{
private:
    std::unique_ptr<ProgrammerRepositoryInterface> m_programmerRepo;
    std::unique_ptr<SourceFileRepositoryInterface> m_sourceFileRepo;

public:
    CodeRevisionControllerImpl();

public: // CodeRevisionControllerInterface interface
    const std::vector<SourceFile>& getSourceFiles();
    const std::vector<Programmer>& getProgrammers();
    void addSourceFile(const std::string& fileName, const Programmer& programmer);
    void reviewSourceFile(const std::string& fileName, const Programmer& programmer);
};

#endif // CODEREVISIONCONTROLLERIMPL_HPP
