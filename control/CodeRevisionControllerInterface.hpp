#ifndef CODEREVISIONCONTROLLERINTERFACE_HPP
#define CODEREVISIONCONTROLLERINTERFACE_HPP

#include <string>
#include <vector>

class SourceFile;
class Programmer;

class CodeRevisionControllerInterface
{
public:
    virtual const std::vector<SourceFile>& getSourceFiles() = 0;
    virtual const std::vector<Programmer>& getProgrammers() = 0;
    virtual void addSourceFile(const std::string& fileName, const Programmer& programmer) = 0;
    virtual void reviewSourceFile(const std::string& fileName, const Programmer& programmer) = 0;

    virtual ~CodeRevisionControllerInterface() = default;
};

#endif // CODEREVISIONCONTROLLERINTERFACE_HPP
