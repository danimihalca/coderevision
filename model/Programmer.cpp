#include "Programmer.hpp"

Programmer::Programmer(const std::string name, int noRevisedFiles, int noOfRemainingFilesToRevise)
    : m_name(name)
    , m_noRevisedFiles(noRevisedFiles)
    , m_noOfRemainingFilesToRevise(noOfRemainingFilesToRevise)
{
}

const std::string& Programmer::getName() const
{
    return m_name;
}

void Programmer::setName(const std::string& name)
{
    m_name = name;
}

int Programmer::getNoRevisedFiles() const
{
    return m_noRevisedFiles;
}

void Programmer::setNoRevisedFiles(int noRevisedFiles)
{
    m_noRevisedFiles = noRevisedFiles;
}

int Programmer::getNoOfRemainingFilesToRevise() const
{
    return m_noOfRemainingFilesToRevise;
}

void Programmer::setNoOfRemainingFilesToRevise(int noOfRemainingFilesToRevise)
{
    m_noOfRemainingFilesToRevise = noOfRemainingFilesToRevise;
}
