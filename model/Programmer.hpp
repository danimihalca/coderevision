#ifndef PROGRAMMER_HPP
#define PROGRAMMER_HPP

#include <string>

class Programmer
{
private:
    std::string m_name;
    int m_noRevisedFiles;
    int m_noOfRemainingFilesToRevise;

public:
    Programmer(const std::string name, int noRevisedFiles, int noOfRemainingFilesToRevise);

    const std::string& getName() const;
    void setName(const std::string &name);

    int getNoRevisedFiles() const;
    void setNoRevisedFiles(int noRevisedFiles);

    int getNoOfRemainingFilesToRevise() const;
    void setNoOfRemainingFilesToRevise(int noOfRemainingFilesToRevise);
};

#endif // PROGRAMMER_HPP
