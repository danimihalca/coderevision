#include "SourceFile.hpp"

SourceFile::SourceFile(const std::string& name, SourceFileStatus status, const std::string& creator, const std::string& reviewer)
    : m_name(name)
    , m_status(status)
    , m_creator(creator)
    , m_reviewer(reviewer)
{
}

const std::string& SourceFile::getName() const
{
    return m_name;
}

void SourceFile::setName(const std::string& name)
{
    m_name = name;
}

SourceFileStatus SourceFile::getStatus() const
{
    return m_status;
}

void SourceFile::setStatus(const SourceFileStatus& status)
{
    m_status = status;
}

const std::string& SourceFile::getCreator() const
{
    return m_creator;
}

void SourceFile::setCreator(const std::string& creator)
{
    m_creator = creator;
}

const std::string& SourceFile::getReviewer() const
{
    return m_reviewer;
}

void SourceFile::setReviewer(const std::string& reviewer)
{
    m_reviewer = reviewer;
}
