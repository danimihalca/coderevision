#ifndef SOURCEFILE_HPP
#define SOURCEFILE_HPP

#include <string>

enum SourceFileStatus
{
    REVISED,
    NOT_REVISED
};

class SourceFile
{
private:
    std::string m_name;
    SourceFileStatus m_status;
    std::string m_creator;
    std::string m_reviewer;

public:
    SourceFile(const std::string& name, SourceFileStatus status, const std::string& creator, const std::string& reviewer);

    const std::string& getName() const;
    void setName(const std::string& name);

    SourceFileStatus getStatus() const;
    void setStatus(const SourceFileStatus& status);

    const std::string& getCreator() const;
    void setCreator(const std::string& creator);

    const std::string& getReviewer() const;
    void setReviewer(const std::string& reviewer);
};

#endif // SOURCEFILE_HPP
