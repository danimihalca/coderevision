#include "FileProgrammerRepository.hpp"

#include <iostream>
#include <fstream>
#include <sstream> // for splitting

const char FileProgrammerRepository::s_DELIMITER = '|';

FileProgrammerRepository::FileProgrammerRepository(const std::string& fileName)
    : m_fileName(fileName)
    , m_programmers()
{
    loadFromFile();
}

FileProgrammerRepository::~FileProgrammerRepository()
{
    saveToFile();
}

const std::vector<Programmer>& FileProgrammerRepository::getProgrammers()
{
    return m_programmers;
}

bool FileProgrammerRepository::programmerExists(const std::string& aName)
{
    for (const auto& programmer: m_programmers)
    {
        if (programmer.getName().compare(aName))
        {
            return true;
        }
    }

    return false;
}

void FileProgrammerRepository::loadFromFile()
{
    std::ifstream file(m_fileName, std::fstream::in);

    if (!file.is_open())
    {
        std::cout << "could not open file " << m_fileName << "for writing/appending" << std::endl;
        return;
    }

    while (!file.eof())
    {
        std::string line;
        file >> line;

        if (line.empty())
        {
            continue;
        }

        std::istringstream lineStream(line);

        std::string token;

        std::string name;
        int noRevisedFiles;
        int noRemainingFilesToRevise;

        if (!std::getline(lineStream, token, s_DELIMITER))
        {
            std::cout << "could not read 1st token from programmers file" << std::endl;
        }

        name = token;

        if (!std::getline(lineStream, token, s_DELIMITER))
        {
            std::cout << "could not read 2nd token from programmers file" << std::endl;
        }

        try
        {
            noRevisedFiles = std::stoi(token);
        }
        catch (...)
        {
            std::cout<< "Invalid number for noRevisedFiles " << std::endl;
        }

        if (!std::getline(lineStream, token, s_DELIMITER))
        {
            std::cout << "could not read 3rd token from programmers file" << std::endl;
        }

        try
        {
            noRemainingFilesToRevise = std::stoi(token);
        }
        catch (...)
        {
            std::cout<< "Invalid number for noRemainingFilesToRevise " << std::endl;
        }

        if (std::getline(lineStream, token, s_DELIMITER))
        {
            std::cout << "too many tokens in programmers file" << std::endl;
        }

        m_programmers.push_back(Programmer(name, noRevisedFiles, noRemainingFilesToRevise));
    }
}

void FileProgrammerRepository::saveToFile()
{
    std::ofstream file(m_fileName, std::fstream::out);

    if (!file.is_open())
    {
        std::cout<< "could not open file " << m_fileName << "for writing/appending" << std::endl;
        return;
    }

    for (const auto& programmer: m_programmers)
    {
        file << programmer.getName()                       << s_DELIMITER <<
                programmer.getNoRevisedFiles()             << s_DELIMITER <<
                programmer.getNoOfRemainingFilesToRevise() << std::endl;

    }

    file.close();
}
