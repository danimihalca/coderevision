#ifndef FILEPROGRAMMERREPOSITORY_HPP
#define FILEPROGRAMMERREPOSITORY_HPP

#include "ProgrammerRepositoryInterface.hpp"

#include "model/Programmer.hpp"

class FileProgrammerRepository: public ProgrammerRepositoryInterface
{
private:
    std::string m_fileName;
    std::vector<Programmer> m_programmers;

    static const char s_DELIMITER;

public:
    FileProgrammerRepository(const std::string& fileName);
    ~FileProgrammerRepository();

public:  // ProgrammerRepositoryInterface interface
    const std::vector<Programmer>& getProgrammers();
    bool programmerExists(const std::string& aName);

private:
    void loadFromFile();
    void saveToFile();

};

#endif // FILEPROGRAMMERREPOSITORY_HPP
