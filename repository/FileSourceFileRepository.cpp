#include "FileSourceFileRepository.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

const char FileSourceFileRepository::s_DELIMITER = '|';

FileSourceFileRepository::FileSourceFileRepository(const std::string& fileName)
    : m_fileName(fileName)
    , m_sourceFiles()
{
    loadFromFile();
}

FileSourceFileRepository::~FileSourceFileRepository()
{
    saveToFile();
}

void FileSourceFileRepository::addSourceFile(const SourceFile& aSourceFile)
{
    m_sourceFiles.push_back(aSourceFile);
}

void FileSourceFileRepository::updateSourceFile(const SourceFile& aSourceFile)
{
    for (auto& sourceFile: m_sourceFiles)
    {
        if (sourceFile.getName().compare(aSourceFile.getName()) == 0)
        {
            sourceFile.setStatus(aSourceFile.getStatus());
            sourceFile.setReviewer(aSourceFile.getReviewer());
        }
    }
}

const std::vector<SourceFile>& FileSourceFileRepository::getSourceFiles()
{
    return m_sourceFiles;
}

bool FileSourceFileRepository::sourceFileExists(const std::string& aName)
{
    for (auto& sourceFile: m_sourceFiles)
    {
        if (sourceFile.getName().compare(aName) == 0)
        {
            return true;
        }
    }

    return false;
}

void FileSourceFileRepository::loadFromFile()
{
    std::ifstream file(m_fileName, std::fstream::in);

    if (!file.is_open())
    {
        std::cout << "could not open file " << m_fileName << " for writing/appending" << std::endl;
        return;
    }

    while (!file.eof())
    {
        std::string line;
        file >> line;

        if (line.empty())
        {
            continue;
        }

        std::istringstream lineStream(line);

        std::string token;

        std::string name;
        SourceFileStatus status = SourceFileStatus::NOT_REVISED;
        std::string creator;
        std::string reviewer;

        std::getline(lineStream, token, s_DELIMITER);
        name = token;

        std::getline(lineStream, token, s_DELIMITER);
        if (token.compare("REVISED") == 0)
        {
            status = SourceFileStatus::REVISED;
        }

        std::getline(lineStream, token, s_DELIMITER);
        creator = token;

        std::getline(lineStream, token, s_DELIMITER);
        reviewer = token;

        m_sourceFiles.push_back(SourceFile(name, status, creator, reviewer));
    }
}

void FileSourceFileRepository::saveToFile()
{
    std::ofstream file(m_fileName, std::fstream::out);

    if (!file.is_open())
    {
        std::cout<< "could not open file " << m_fileName << " for writing/appending" << std::endl;
        return;
    }

    for (const auto& sourceFile: m_sourceFiles)
    {
        file << sourceFile.getName()                                                              << s_DELIMITER <<
                (sourceFile.getStatus() == SourceFileStatus::REVISED ? "REVISED" : "NOT_REVISED") << s_DELIMITER <<
                sourceFile.getCreator()                                                           << s_DELIMITER <<
                sourceFile.getReviewer()                                                          << std::endl;
    }

    file.close();
}
