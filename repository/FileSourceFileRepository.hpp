#ifndef FILESOURCEFILEREPOSITORY_HPP
#define FILESOURCEFILEREPOSITORY_HPP

#include "SourceFileRepositoryInterface.hpp"

#include "model/SourceFile.hpp"

class FileSourceFileRepository : public SourceFileRepositoryInterface
{
private:
    std::string m_fileName;
    std::vector<SourceFile> m_sourceFiles;

    static const char s_DELIMITER;

public:
    FileSourceFileRepository(const std::string& fileName);
    ~FileSourceFileRepository();

public: // SourceFileRepositoryInterface interface
    void addSourceFile(const SourceFile& aSourceFile);
    void updateSourceFile(const SourceFile& aSourceFile);
    const std::vector<SourceFile>& getSourceFiles();
    bool sourceFileExists(const std::string& aName);

private:
    void loadFromFile();
    void saveToFile();
};

#endif // FILESOURCEFILEREPOSITORY_HPP
