#ifndef PROGRAMMERREPOSITORYINTERFACE_HPP
#define PROGRAMMERREPOSITORYINTERFACE_HPP

#include <vector>
#include <string>

// Forward declarations;
class Programmer;

class ProgrammerRepositoryInterface
{
public:
    virtual const std::vector<Programmer>& getProgrammers() = 0;
    virtual bool programmerExists(const std::string& aName) = 0;

    virtual ~ProgrammerRepositoryInterface() = default;
};

#endif // PROGRAMMERREPOSITORYINTERFACE_HPP
