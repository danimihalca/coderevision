#ifndef SOURCEFILEREPOSITORYINTERFACE_HPP
#define SOURCEFILEREPOSITORYINTERFACE_HPP

#include <vector>
#include <string>

// Forward declarations
class SourceFile;

class SourceFileRepositoryInterface
{

public:
    virtual void addSourceFile(const SourceFile& sourceFile) = 0;
    virtual void updateSourceFile(const SourceFile& sourceFile) = 0;
    virtual const std::vector<SourceFile>& getSourceFiles() = 0;
    virtual bool sourceFileExists(const std::string& aName) = 0;

    virtual ~SourceFileRepositoryInterface() = default;
};

#endif // SOURCEFILEREPOSITORYINTERFACE_HPP
