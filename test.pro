TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    model/Programmer.cpp \
    model/SourceFile.cpp \
    repository/FileProgrammerRepository.cpp \
    repository/FileSourceFileRepository.cpp \
    control/CodeRevisionControllerImpl.cpp \
    ui/MainConsoleWindow.cpp \
    ui/ProgrammerConsoleWindow.cpp

HEADERS += \
    model/Programmer.hpp \
    model/SourceFile.hpp \
    repository/ProgrammerRepositoryInterface.hpp \
    repository/FileProgrammerRepository.hpp \
    repository/SourceFileRepositoryInterface.hpp \
    repository/FileSourceFileRepository.hpp \
    control/CodeRevisionControllerInterface.hpp \
    control/CodeRevisionControllerImpl.hpp \
    ui/MainConsoleWindow.hpp \
    ui/ProgrammerConsoleWindow.hpp
