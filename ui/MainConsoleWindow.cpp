#include "MainConsoleWindow.hpp"
#include "ProgrammerConsoleWindow.hpp"

#include "control/CodeRevisionControllerImpl.hpp"

#include "model/Programmer.hpp"

#include <iostream>

MainConsoleWindow::MainConsoleWindow()
    : m_programmerConsoleWindows()
    , m_codeRevisionController(new CodeRevisionControllerImpl)
{
    initialize();
}

void MainConsoleWindow::show()
{
    while (true)
    {
        showMenu();

        int selectedOption = getOption();

        if (selectedOption == 0)
        {
            break;
        }
        else if (selectedOption != -1)
        {
            auto window = m_programmerConsoleWindows.find(selectedOption);

            if (window != m_programmerConsoleWindows.end())
            {
                window->second.show();
            }
            else
            {
                std::cout << "INvalid input" << std::endl;
            }
        }
    }
}

void MainConsoleWindow::initialize()
{
    const auto& programmers = m_codeRevisionController->getProgrammers();

    int index = 1;
    for (const auto& programmer : programmers)
    {
        m_programmerConsoleWindows.emplace(index++, ProgrammerConsoleWindow(m_codeRevisionController, programmer));
    }

}

void MainConsoleWindow::showMenu()
{
    std::cout << "===============================================" << std::endl;

    std::cout << "0) Exit" << std::endl;

    for (const auto& windowPair: m_programmerConsoleWindows)
    {
        std::cout << windowPair.first << ") Window for " << windowPair.second.getProgrammerName() << std::endl;
    }
}

int MainConsoleWindow::getOption()
{
    std::string input;
    int option;

    std::cout << " Select an option:";
    std::cin >> input;

    try
    {
        option = std::stoi(input);
    }
    catch (...)
    {
        std::cout << "INvalid input" << std::endl;

        return -1;
    }

    return option;
}
