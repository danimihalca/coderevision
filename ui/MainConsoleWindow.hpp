#ifndef MAINCONSOLEWINDOW_HPP
#define MAINCONSOLEWINDOW_HPP

#include <memory>
#include <map>

#include "ProgrammerConsoleWindow.hpp"

class CodeRevisionControllerInterface;

class MainConsoleWindow
{
private:
    std::map<int, ProgrammerConsoleWindow> m_programmerConsoleWindows;
    std::shared_ptr<CodeRevisionControllerInterface> m_codeRevisionController;

public:
    MainConsoleWindow();

    void show();


private:
    void initialize();

    void showMenu();
    int getOption();

};

#endif // MAINCONSOLEWINDOW_HPP
