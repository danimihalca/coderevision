#include "ProgrammerConsoleWindow.hpp"

#include "model/Programmer.hpp"
#include "model/SourceFile.hpp"

#include "control/CodeRevisionControllerInterface.hpp"

#include <iostream>

ProgrammerConsoleWindow::ProgrammerConsoleWindow(std::shared_ptr<CodeRevisionControllerInterface> controller,
                                                 const Programmer&                                programmer)
    : m_codeRevisionController(controller)
    , m_programmer(programmer)
{
}

void ProgrammerConsoleWindow::show()
{
    while (true)
    {
        showStatus();
        bool exit = showSourceFiles();
        if (exit)
        {
            break;
        }
    }
}

const std::string& ProgrammerConsoleWindow::getProgrammerName() const
{
    return m_programmer.getName();
}

void ProgrammerConsoleWindow::showStatus()
{
    std::cout << "===============================================" << std::endl;
    std::cout << "Status for:" << m_programmer.getName() << std::endl;

    std::cout << "Number of revised files:" << m_programmer.getNoRevisedFiles() << std::endl;
    std::cout << "Number of remaining files to revise :" << m_programmer.getNoOfRemainingFilesToRevise() << std::endl;
}

bool ProgrammerConsoleWindow::showSourceFiles()
{
    std::cout << "===============================================" << std::endl;
    std::cout << "0) Exit" << std::endl;

    const auto& sourceFiles = m_codeRevisionController->getSourceFiles();
    int index = 1;
    for (const auto& sourceFile: sourceFiles)
    {
        std::cout << index++ << ") REVIEW FOR : " <<
                     sourceFile.getName() << " - " <<
                     sourceFile.getCreator() << " - " <<
                     (sourceFile.getStatus() == SourceFileStatus::REVISED ? "REVISED" : "NOT REVISED") << " - " <<
                     sourceFile.getReviewer() << std::endl;
    }

    int selectedOption = getOption();
    if (selectedOption == 0)
    {
        return true;
    }

    else if (selectedOption < 0 || selectedOption > sourceFiles.size())
    {
        std::cout << "INvalid input" << std::endl;
        return false;
    }

    m_codeRevisionController->reviewSourceFile(sourceFiles.at(selectedOption-1).getName(), m_programmer);

    return false;
}

int ProgrammerConsoleWindow::getOption()
{
    std::string input;
    int option;

    std::cout << " Select an option:";
    std::cin >> input;

    try
    {
        option = std::stoi(input);
    }
    catch (...)
    {
        return -1;
    }

    return option;
}
