#ifndef PROGRAMMERCONSOLEWINDOW_HPP
#define PROGRAMMERCONSOLEWINDOW_HPP

#include <memory>
#include <string>

class CodeRevisionControllerInterface;
class Programmer;

class ProgrammerConsoleWindow
{
private:
    std::shared_ptr<CodeRevisionControllerInterface> m_codeRevisionController;
    const Programmer& m_programmer;

public:
    ProgrammerConsoleWindow(std::shared_ptr<CodeRevisionControllerInterface> controller, const Programmer& programmer);

    void show();
    const std::string& getProgrammerName() const;

private:
    void showStatus();
    bool showSourceFiles();
    int getOption();
};

#endif // PROGRAMMERCONSOLEWINDOW_HPP
